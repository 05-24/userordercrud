package com.devcamp.api.COrderRepository;

import com.devcamp.api.model.CUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CUserRepository extends JpaRepository<CUser,Integer>{
    
}
