package com.devcamp.api.COrderRepository;

import com.devcamp.api.model.COrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface COrderRepository extends JpaRepository<COrder,Integer>{
    
}
