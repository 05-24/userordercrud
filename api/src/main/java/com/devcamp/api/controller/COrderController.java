package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.api.COrderRepository.COrderRepository;
import com.devcamp.api.COrderRepository.CUserRepository;
import com.devcamp.api.model.COrder;
import com.devcamp.api.model.CUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping("/")
@CrossOrigin
public class COrderController {
    
    @Autowired
    CUserRepository userRepository;

    @Autowired
    COrderRepository orderRepository;

    //Lấy tất cả danh sách đơn hàng
    @GetMapping("/orders")
    public ResponseEntity<List<COrder>> getAllOrders(){
        try {
            List<COrder> allOrders = new ArrayList<>();
            orderRepository.findAll().forEach(allOrders::add);
            return new ResponseEntity<>(allOrders,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy một đơn hàng theo id 
    @GetMapping("/orders/{id}")
    public ResponseEntity<COrder> getRegionById(@PathVariable("id") int id){
        try {
            Optional<COrder> findOrder = orderRepository.findById(id);
            if(findOrder.isPresent()){
                COrder orderData = findOrder.get();
                return new ResponseEntity<>(orderData,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy tất cả region của country theo id 
    @GetMapping("/users/{id}/orders")
    public ResponseEntity<List<COrder>> getAllOrdersOfUser(@PathVariable("id") int id){
        try {
            Optional<CUser> findUser = userRepository.findById(id);
            if(findUser.isPresent()){
                CUser userData = findUser.get();
                List<COrder> ordersOfUser = userData.getOrders();
                return new ResponseEntity<>(ordersOfUser,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá tất cả danh sách đơn hàng
    @DeleteMapping("/orders")
    public ResponseEntity<COrder> deleteAllOrders(){
        try {
            orderRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá đơn hàng theo id
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<COrder> deleteOrderById(@PathVariable("id") int id){
        try {
            Optional<COrder> findOrder = orderRepository.findById(id);
            if(findOrder.isPresent()){
                orderRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Tạo mới một đơn hàng theo người dùng bằng id
    @PostMapping("/users/{id}/orders")
    public ResponseEntity<Object> createOrder(@PathVariable("id") int id,@Valid @RequestBody COrder pOrder){
        try {
            Optional<CUser> findUser = userRepository.findById(id);
            if(findUser.isPresent()){
                COrder newOrder = new COrder();
                CUser userData = findUser.get();
                newOrder.setOrderCode(pOrder.getOrderCode());
                newOrder.setCreated(new Date());
                newOrder.setPizzaSize(pOrder.getPizzaSize());
                newOrder.setPizzaType(pOrder.getPizzaType());
                newOrder.setVoucherCode(pOrder.getVoucherCode());
                newOrder.setPaid(pOrder.getPaid());
                newOrder.setPrice(pOrder.getPrice());

                newOrder.setUser(userData);

                COrder saveOrder = orderRepository.save(newOrder);
                return new ResponseEntity<>(saveOrder,HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Update một đơn hàng theo id
    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable("id") int id,@Valid @RequestBody COrder pOrder){
        try {
            Optional<COrder> findOrder = orderRepository.findById(id);
            if(findOrder.isPresent()){
                COrder orderData = findOrder.get();
                orderData.setOrderCode(pOrder.getOrderCode());
                orderData.setUpdated(new Date());
                orderData.setPizzaSize(pOrder.getPizzaSize());
                orderData.setPizzaType(pOrder.getPizzaType());
                orderData.setVoucherCode(pOrder.getVoucherCode());
                orderData.setPaid(pOrder.getPaid());
                orderData.setPrice(pOrder.getPrice());

                COrder saveOrder = orderRepository.save(orderData);
                return new ResponseEntity<>(saveOrder,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Hàm đếm tất cả đơn hàng
    @GetMapping("/orders/count")
    public ResponseEntity<Long> countOrder(){
        try {
            Long count = orderRepository.count();
            return new ResponseEntity<Long>(count,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Hàm đếm tất cả regions trong một country theo id
    @GetMapping("/users/{id}/orders/count")
    public ResponseEntity<Integer> countOrderOfUser(@PathVariable("id") int id){
        try {
            Optional<CUser> findUser = userRepository.findById(id);
            if(findUser.isPresent()){
                CUser userData = findUser.get();
                List<COrder> allOrdersOfUser = userData.getOrders();
                Integer count = allOrdersOfUser.size();
                return new ResponseEntity<>(count,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Hàm kiểm tra region có id có tồn tại không
    @GetMapping("/orders/{id}/check")
    public ResponseEntity<Boolean> checkOrder(@PathVariable("id") int id){
        try {
            Optional<COrder> findOrder = orderRepository.findById(id);
            if(findOrder.isPresent()){
                return new ResponseEntity<>(true,HttpStatus.FOUND);
            } else {
                return new ResponseEntity<>(false,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
