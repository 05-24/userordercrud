package com.devcamp.api.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.devcamp.api.COrderRepository.CUserRepository;
import com.devcamp.api.model.CUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.util.Date;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CUserController {
    
    @Autowired
    CUserRepository userRepository;

    //Lấy danh sách tất cả người dùng
    @GetMapping("/users")
    public ResponseEntity<List<CUser>> getAllUsers(){
        try {
            List<CUser> allUsers = new ArrayList<>();
            userRepository.findAll().forEach(allUsers::add);
            return new ResponseEntity<>(allUsers,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy một người dùng theo id
    @GetMapping("/users/{id}")
    public ResponseEntity<CUser> getUserById(@PathVariable("id") int id){
        try {
            Optional<CUser> findUser = userRepository.findById(id);
            if(findUser.isPresent()){
                CUser userData = findUser.get();
                return new ResponseEntity<CUser>(userData,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá tất cả người dùng 
    @DeleteMapping("/users")
    public ResponseEntity<CUser> deleteAllUsers(){
        try {
            userRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá một country theo id
    @DeleteMapping("/users/{id}")
    public ResponseEntity<CUser> deleteUserById(@PathVariable("id") int id){
        try {
            userRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }  catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Tạo mới một người dùng 
    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@Valid @RequestBody CUser pUser){
        try {
            CUser newUser = new CUser();
            newUser.setAddress(pUser.getAddress());
            newUser.setEmail(pUser.getEmail());
            newUser.setFullname(pUser.getFullname());
            newUser.setOrders(pUser.getOrders());
            newUser.setPhone(pUser.getPhone());
            newUser.setCreated(new Date());

            CUser saveUser = userRepository.save(newUser);
            return new ResponseEntity<>(saveUser,HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Update một user 
    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable("id") int id,@Valid @RequestBody CUser pUser){
        try {
            Optional<CUser> findUser = userRepository.findById(id);
            if(findUser.isPresent()){
                CUser userData = findUser.get();
                userData.setAddress(pUser.getAddress());
                userData.setEmail(pUser.getEmail());
                userData.setFullname(pUser.getFullname());
                userData.setOrders(pUser.getOrders());
                userData.setPhone(pUser.getPhone());
                userData.setUpdated(new Date());

                CUser saveUser = userRepository.save(userData);
                return new ResponseEntity<>(saveUser,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Hàm đếm số country
    @GetMapping("/users/count")
    public ResponseEntity<Long> countUser(){
        try {
            Long count = userRepository.count();
            return new ResponseEntity<Long>(count,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    //Hàm kiểm tra xem country có id có tồn tại không ?
    @GetMapping("/users/{id}/check")
    public ResponseEntity<Boolean> checkUser(@PathVariable("id") int id){
        try {
            Optional<CUser> findCountry = userRepository.findById(id);
            if(findCountry.isPresent()){
                return new ResponseEntity<>(true,HttpStatus.FOUND);
            } else {
                return new ResponseEntity<>(false,HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
